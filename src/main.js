// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

/* Vuetify*/
import Vuetify from 'vuetify'
Vue.use(Vuetify)
//require('/path/to/node_modules/vuetify/dist/vuetify.min.css')

/* Vue material*/
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'
Vue.use(VueMaterial)

/* Componentes customizáveis*/
import CadUsuario from './components/usuario/cadUsuario'


Vue.component('cadUsuario', CadUsuario);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
